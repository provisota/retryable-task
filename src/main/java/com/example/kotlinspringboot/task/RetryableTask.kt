package com.example.kotlinspringboot.task

import org.springframework.retry.annotation.Backoff
import org.springframework.retry.annotation.Recover
import org.springframework.retry.annotation.Retryable
import org.springframework.stereotype.Component

/**
 * @author Ilya Alterovych
 */

@Component
interface RetryableTask {

    @Retryable(
            value = [TaskRuntimeException::class],
            maxAttemptsExpression = "#{\${task.eligibility.retry.backoff.maxAttempts}}",
            backoff = Backoff(
                    delayExpression = "#{\${task.eligibility.retry.backoff.delay}}",
                    maxDelayExpression = "#{\${task.eligibility.retry.backoff.maxDelay}}",
                    multiplierExpression = "#{\${task.eligibility.retry.backoff.multiplier}}"
            )
    )
    fun retryableTask()

    @Recover
    fun recover(ex: TaskRuntimeException)
}