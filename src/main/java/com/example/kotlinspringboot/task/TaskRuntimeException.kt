package com.example.kotlinspringboot.task

/**
 * @author Ilya Alterovych
 */
class TaskRuntimeException(message: String? = null) : RuntimeException(message)