package com.example.kotlinspringboot.task

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

/**
 * @author Ilya Alterovych
 */

@Component
open class RetryableTaskImpl : RetryableTask {

    private var previousMills = 0L
    private var attemptNumber = 0

    @Scheduled(cron = "\${task.eligibility.cron.expression}")
    override fun retryableTask() {
        if (previousMills == 0L) previousMills = System.currentTimeMillis()

        attemptNumber++

        val delay = (System.currentTimeMillis() - previousMills).toDouble() / 1000.0

        println("attemptNumber: $attemptNumber, delay: $delay")

        previousMills = System.currentTimeMillis()

        if (attemptNumber < 21) throw TaskRuntimeException("Max num retries reached.")

        println("Task successfully finished.")
    }

    override fun recover(ex: TaskRuntimeException) =
            println("${this::class.java.simpleName} task failed with root cause: ${ex.localizedMessage}")
}